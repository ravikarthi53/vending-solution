//import javax.swing.JOptionPane;


import com.ibm.mqtt.IMqttClient;
import com.ibm.mqtt.MqttClient;
import com.ibm.mqtt.MqttException;
import com.ibm.mqtt.MqttPersistence;
import com.ibm.mqtt.MqttSimpleCallback;
//import com.ibm.mqtt.MqttBaseClient;
//import MqttFilePersistence;
//import for pushing data

/**
 * 
 */

/**
 * @author kamal.t
 *
 */
public class MqttJavaSubsClient implements MqttSimpleCallback, Runnable {

	private IMqttClient mqtt = null;
	private boolean connected = false;
	private boolean traceEnabled = false;
	private Object    connLostWait = new Object(); // Object to coordinated ConnectionLost and disconnect threads if
                	                                // disconnect is hit during connectionLost
	public SavingToQueue savequeue=new SavingToQueue();
	//private byte[] fileContent = null;
	
	/**
	 * 
	 */
	public MqttJavaSubsClient() {
		//MqttJavaSubsClient  subsObj = new MqttJavaSubsClient();
		//new Thread(subsObj).start();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MqttJavaSubsClient subsObj = new MqttJavaSubsClient();
		try
		{
		subsObj.connect("tcp://127.0.0.1:1883", false,"0009");
		subsObj.connected=true;
		new Thread(subsObj).start();
		 
		}
		catch(Exception e)
		{
			
		}
		//UserAuthentication userauth=new UserAuthentication();
		//MqttTopic mqt1=new MqttTopic();
		
		//subsObj.subpub(subsObj);
		}
		
		//MqttJavaSubsClient subsObj = new MqttJavaSubsClient();
		//new Thread(subsObj).start();
	

	
    /**
     * A wrapper for the MQTT connect method. If the ip address, port number or persistence flag
     * has changed since the last time then a new MqttClient object is required. If these values haven't changed then
     * any previously created object can be used.<P>
     * Check whether Last Will & Testament is required and call the appropriate connect method. The only persistence implementation supported at the moment is
     * MqttFilePersistence.
     * @param ipAddr The IP address or hostname to connect to.
     * @param port The IP port number to connect to.
     * @param usePersistence Is persistence required?
     */
    public void connect( String connStr, boolean usePersistence,String id ) throws MqttException {
    	//System.out.println(connStr);
		// Connect to the broker
    	// If we have a MqttClient object and the new ip address
		// or port number is not equal to the previous, or the persistence flag changes between
		// off and on then we need a new object.
		if ( (mqtt != null) &&
		     (!connStr.equals(mqtt.getConnection()) ||
		     (usePersistence != (mqtt.getPersistence() != null) ) ) ) {
			mqtt.terminate();
			mqtt = null;
		}	
		if ( mqtt == null ) {
			MqttPersistence persistence = null;
			if ( usePersistence ) {
				persistence = new MqttFilePersistence(".");
			}	
        	mqtt = MqttClient.createMqttClient( connStr, persistence );
        	
    	    mqtt.registerSimpleHandler( this );
    	    // Carry the trace setting over to the new MqttClient object
    	    if ( traceEnabled ) {
    	    	startTrace();
            }
		}	
        
        // Set the retry interval for the connection
        mqtt.setRetry(10);
        
        if ( true ) {
        	mqtt.connect(id, true, (short)30);
        } 
    }

    /**
     * This method is called when either the subscribe or unsubscribe buttons are pressed. It performs the 
     * WMQTT subscribe or unsubscribe and writes an entry in the history log if the history window is open.<BR>
     * Any exceptions are caught and displayed in a dialog box.
     * @param topic The topic to subscribe to
     * @param qos The maximum Quality of Service at which to receive publications
     * @param sub Is this a subscribe or unsubscribe operation? (true if subscribe).
     */     			
    public void subscription( String topic, int qos, boolean sub ) {
		// setTitleText( "" );

    	if ( connected ) {
    		try {
       		    String[] theseTopics = new String[1];
    		    int[] theseQoS = new int[1];
    		  
    		    theseTopics[0] = topic;
    		    theseQoS[0] = qos;
    		    //theseTopics[1]="Deneebo/Sakthi/Dinamalar";
    		   // theseQoS[1]=10;
        	    synchronized(this) { // Grab the log synchronisation lock
                  if ( sub ) {
            	      writeLogln( "  --> SUBSCRIBE,        TOPIC:" + topic + ", Requested QoS:" + qos );
                  } else {
                	  writeLogln( "  --> UNSUBSCRIBE,      TOPIC:" + topic );
                  }	  
        	    }
        	      
                if ( sub ) {
        		    mqtt.subscribe( theseTopics, theseQoS );
                } else {
                    mqtt.unsubscribe( theseTopics );
                }	  
    		  
    		} catch ( Exception ex ) {
    			System.out.println( "MQTT subscription exception caught !" );
    			System.out.println(ex.getMessage() + "\n" + "MQTT Subscription Exception");
    		}	
    	} else {
    		System.out.println( "MQTT client not connected !" );
    	}		
    }	
    

    /**
     * A simple wrapper for the WMQTT publish method. This method is invoked as a result of the
     * publish button being pressed. If a problem is detected then an exception is thrown and an error
     * message is displayed in the window title bar or in a separate dialog box.
     * @param topic The topic on which the data will be published.
     * @param message The data to be published
     * @param qos The Quality of Service at which the publication should be delivered.
     * @param retained Is this a retained publication or not?
     */
    public void publish( String topic, byte[] message, int qos, boolean retained ) throws Exception {
		// setTitleText( "" );

    	if ( true ) {
    		try {
    		    mqtt.publish( topic, message, qos, retained );
    		} catch ( MqttException ex ) {
        		// setTitleText( "MQTT publish exception !" );
        		System.out.println(ex.getClass().getName() + "\n" + ex.getMessage() + "\n" + "MQTT Publish Exception");
        		throw ex;
    		}	
    	} 
    }	
    
    /**
     * A wrapper for the WMQTT disconnect method.
     * As well as disconnecting the protocol this method enables / disables buttons
     * as appropriate when in disconnected state. It also sets the correct colour of the indicator LED.
     */
    public void disconnect() {
		connected = false;

        // Notify connectionLost to give up. It may be running..
		synchronized(connLostWait) {
   			connLostWait.notify();
		}

		// Disconnect from the broker
		if ( mqtt != null ) {
  			try {
  				mqtt.disconnect();
   			} catch ( Exception ex ) {
    			// setTitleText( "MQTT disconnect error !" );
   				ex.printStackTrace();
   				System.exit(1);
   			}	 
   		}		

        // Set the LED state correctly
		// If the led is flashing then turn it off
		// This only occurs if disconnect is hit during connection lost
		/*
		if ( led.isFlashing() ) {
			led.setFlash();
		}	
		led.setRed();
		*/
		
        // setConnected( false );
		
   	    synchronized(this) { // Grab the log synchronisation lock
     		writeLogln("WebSphere MQ Telemetry transport disconnected" );
		}	
    }	
    

    /**
     * The method is part of the MqttSimpleCallback interface
     * <BR>In the event of the WMQTT connection being broken the LED is set to colour amber and made to flash.
     * The code then keeps trying to reconnect until either a successful
     * reconnect occurs or the disconnect button is pressed. Finally the LED is stopped flashing and set to 
     * green or red depending upon whether the connect was successful or not.
     */
    public void connectionLost() throws Exception {
    	int rc = -1;
    	
    	// Flip the LED to Amber and set it flashing
    	// led.setAmber();
    	// led.setFlash();
    	
    	// setTitleText( "Connection Lost!....Reconnecting" );
   	    synchronized(this) { // Grab the log synchronisation lock
     		writeLogln( "MQTT Connection Lost!....Reconnecting to " + mqtt.getConnection() );
   	    }	

    	try {
    		// While we have failed to reconnect and disconnect hasn't
    		// been called by another thread retry to connect
    		while ( (rc == -1) &&  connected ) {
    			
           		try {
           			synchronized(connLostWait) {
               			connLostWait.wait(10000);
           			}
           		} catch (InterruptedException iex) {
       	    		// Don't care if we are interrupted
       		    }		

        	    synchronized(this) { // Grab the log synchronisation lock
        	    	if ( connected ) {
        	    		writeLog( "MQTT reconnecting......" );
        	    		try {
        	    			connect( mqtt.getConnection(), (mqtt.getPersistence() != null),"0001");
        	    			rc = 0;
        	    		} catch (MqttException mqte) {
        	    			// Catch any MQTT exceptions, set rc to -1 and retry
        	    			rc = -1;
        	    		}		
        	    		if ( rc == -1 ) {
        	    			writeLogln( "failed" );
        	    		} else {
        	    			writeLogln( "success !" );
        	    			subscription("Deneebo/Sakthi/Dinamalar", 2, true );
        	    		}		
        	    	}
        	    }
    		}	
    		// Remove title text once we have reconnected
        	// setTitleText( "" );
    	} catch (Exception ex) {
    		// setTitleText( "MQTT connection broken !" );
    		ex.printStackTrace();
    		disconnect();
    		throw ex;
    	} finally {
    		// Set the flashing off whatever happens
    		/*
    		if ( led.isFlashing() ) {
            	led.setFlash(); // Flash off
    		}
    		*/	
    	}	
    	
    	// If we get here and we are connected then set the led to green
    	if ( connected ) {
        	// led.setGreen();
    		// setConnected( true );
    	} else {
    		// led.setRed();
    		// setConnected( false );
    	}	
    }	
    
    /**
     * The method is part of the MqttSimpleCallback interface<BR>
     * Pass the message as is to the SubPanel object which will display it.
     */
    public void publishArrived( String topic, byte[] data, int QoS, boolean retained ) {
    	
    	try {
			updateReceivedData( topic, data, QoS, retained );
		} catch (Exception e) {
			
			e.printStackTrace();
		}
    }	

    
    /**
     *  Write to the history dialog window and append a newline character after the text
     * @param logdata The line of text to display in the history log
     */
    public void writeLogln( String logdata) {
    	writeLog( logdata + System.getProperty("line.separator") );
    }

    /**
     *  Write to the history dialog window
     * @param logdata The line of text to display in the history log
     */
    public void writeLog( String logdata) {
    	System.out.println(logdata);
    }
    

    /**
     * This method calls the WMQTT startTrace method to produce trace of the protocol flows
     */    
    public void startTrace() throws MqttException {
    	traceEnabled = true;
    	if ( mqtt != null ) {
    		try {
            	mqtt.startTrace();
    		} catch ( MqttException mqe ) {
    			traceEnabled = false;
    			Throwable e = mqe.getCause();
    			if ( e == null ) {
    				e = mqe;
    			}
         		System.out.println(e + "\n" + 
            		 "MQTT start trace exception !");
    			throw mqe;
    		}		
    	}	
    }	

    /**
     * This method calls the WMQTT stopTrace method to stop trace of the protocol flows
     */    
    public void stopTrace() {
    	traceEnabled = false;
    	if ( mqtt != null ) {
        	mqtt.stopTrace();
     		System.out.println("Trace file mqe0.trc generated in the current directory" + "\n" +
        		 "MQTT Trace");
    	}	
    }	
        

    /**
     * This method is passed a received publication. It then:
     * <UL><LI>Switches the display to text if was in hex display mode
     * <LI>Updates the display with the new data
     * <LI>Switches the display back to hex if was originally in hex display mode
     * <LI>Writes a log entry to the history window
     * </UL>
     * @throws Exception 
     */
    public void updateReceivedData( String topic, byte[] data, int QoS, boolean retained ) throws Exception {
    	// Remember the display state (hex or text) of the subscribe JTextArea
    	boolean writeHex = false;
    	
    	// Switch to a character display before adding text
    	if ( writeHex == true ) {
        	toCharString(new String(data));
    	}	
    	
    	//System.out.println( topic );
    //	System.out.println( Integer.toString(QoS) );
    	// receivedRetain.setSelected( retained );
    	//System.out.println(new String(data));
    	if(topic.equals("Deneebo/Sakthi/Dinamalar"))
    	{
    		savequeue.AddToQueue(topic,new String(data));
    	}
    	 // this.subpub();
     	// Store the data content in a buffer incase in needs to be written to a file
     	// If the data is binary reading it back from the receivedData field is not good enough
     	//fileContent = data;
     	
     	
     	// If the display was originally in hex then switch back to hex
     	if ( writeHex ) {
     		toHexString(new String(data));
     	}	
     	
     	// When writing the data to the log get it from the receivedData text area, so that it is in the correct format - Hex or Text
   	    synchronized( this ) { // Grab the log synchronisation lock
   	    	//code for pushing data
   	    	//MessageBroker msgBroker = MessageBroker.getMessageBroker(null);
			//String clientID = UUIDUtils.createUUID();
			//AsyncMessage msg = new AsyncMessage();
			//msg.setDestination("feed");
			//msg.setClientId(clientID);
			//msg.setMessageId(UUIDUtils.createUUID());
			//msg.setTimestamp(System.currentTimeMillis());
			//msg.setBody(new Double(QoS));
			//msgBroker.routeMessageToService(msg, null);
   	    	
         //	writeLogln( "  --> PUBLISH received, TOPIC:" + topic + ", QoS:" + QoS + ", Retained:" + retained );
        	//writeLog( "                        DATA:" );
    	    if ( writeHex ) {
        		// Prefix hex data with 0x
            	writeLog( "0x" );
    	    }	
        	// writeLogln( receivedData.getText() );
   	    }	
     	
    }	
    
    /**
     * This method reads in the data from the received publication text area as text characters and converts them into hex characters (i.e. every character read is represented as two hex characters). It is used when the button
     * saying 'Hex' is pressed indicating that the text data in the data area needs to be converted to a hex representation.<BR>
     * The text string read in from the data area is converted into an array of bytes. The integer value of each byte is then converted into a hex string representation and appended to the output string buffer. Once the entire input string
     * has been processed the output string is written back to the text area.
     */
    private void toHexString(String  receivedData) {
    	String subText = receivedData;
    	StringBuffer hexText = new StringBuffer();
    	
    	byte[] subBytes = subText.getBytes();

    	for( int i=0; i<subBytes.length; i++ ) {
    		int byteValue = subBytes[i];

            // Change the byte value from a signed to unsigned value
            // e.g. A byte of value 0xAA is treated as -86 and displayed incorrectly as 0xFFFFFFAA
            // Adding 256 to this value changes it to 170 which is displayed correctly as 0xAA
    		if (byteValue < 0) {
    			byteValue += 256;
    		}	

    		if ( byteValue < 16 ) {
        		hexText.append( "0" + Integer.toHexString(byteValue) );
    		} else {
        		hexText.append( Integer.toHexString(byteValue) );
    		}		
    	}	
    	
        // hexDisplay = true;
        System.out.println(" - hexadecimal display" );
        // mqttMgr.setTitleText("");
        // hexButton.setText("Text");
    	// receivedData.setText( hexText.toString() );
    }	
    
    /**
     * This method reads in the data from the received publications text area as hex and converts it into a text string (i.e. every two characters read are treated as hex and represent one char). It is used when the button
     * saying 'Text' is pressed indicating that the hex data in the data area needs to be converted to character data.<BR>
     * Error conditions checked for include an odd number of hex characters and invalid base 16 characters (not in [0..9],[A..F]).<BR>
     * The hex input string is converted into a text character array then a new text string is generated from the character array and set as the string in the publication data area.
     */
    private void toCharString(String receivedData) {
    	String hexText = receivedData;
    	
    	if ( hexText.length() % 2 != 0 ) {
    		System.out.println( "Hex length" + hexText.length() );
    		System.out.println( "Odd number of hex characters!" );
    	} else {
    		try {
                byte[] charArray = new byte[hexText.length()/2];    
        		for( int i=0; i<charArray.length; i++ ) {
                    // Take each pair of bytes from the input and convert to a character			
                    // Use the Integer parseInt method to take a 2 byte string in base 16 and turn it into an integer.
    	    		charArray[i] = (byte)Integer.parseInt( hexText.substring( i*2,(i*2)+2), 16 );
    		    }	

                // hexDisplay = false;
                // subLabel.setText( PANEL_TITLE + " - text display" );
    			// mqttMgr.setTitleText( "" );
                // hexButton.setText("Hex");
    		    // receivedData.setText( new String(charArray) );
    		} catch( NumberFormatException nfe ) {

    			System.out.println( "Invalid hexadecimal data!" );
    		}	    
    	}	
    }	
    

    /** Invoked by actionPerformed when connect is pressed. 
     *  This allows actionPerformed to return and paint the window. This thread
     *  then does the MQTT connect to the broker.<BR>
     *  This method also ensures that the LED colour is set correctly and writes
     *  an entry to the history dialog if it is open.
     */
    public void run() {
    	
    	
		// Connect to the broker
		
   	    synchronized(this) { // Grab the log synchronisation lock
   			if ( this.connected ) {
         		writeLogln("WebSphere MQ Telemetry transport connected to " + mqtt.getConnection() );
                subscription("Deneebo/Sakthi/Dinamalar", 2, true );
                
                
   	   		} else {
   	      		//writeLogln("ERROR:WebSphere MQ Telemetry transport failed to connect to " + connStr );
    	    }	
        }    
   	
    }
    		

    /**
     * This method is called when either the subscribe or unsubscribe buttons are pressed. It performs the 
     * WMQTT subscribe or unsubscribe and writes an entry in the history log if the history window is open.<BR>
     * Any exceptions are caught and displayed in a dialog box.
     * @param topic The topic to subscribe to
     * @param qos The maximum Quality of Service at which to receive publications
     * @param sub Is this a subscribe or unsubscribe operation? (true if subscribe).
     */     			
    public void subscription2( String topic, int qos, boolean sub ) {
		//setTitleText( "" );

    	if ( connected ) {
    		try {
       		    String[] theseTopics = new String[1];
    		    int[] theseQoS = new int[1];
    		  
    		    theseTopics[0] = topic;
    		    theseQoS[0] = qos;

        	    synchronized(this) { // Grab the log synchronisation lock
                  if ( sub ) {
            	      writeLogln( "  --> SUBSCRIBE,        TOPIC:" + topic + ", Requested QoS:" + qos );
                  } else {
                	  writeLogln( "  --> UNSUBSCRIBE,      TOPIC:" + topic );
                  }	  
        	    }
        	      
                if ( sub ) {
        		    mqtt.subscribe( theseTopics, theseQoS );
                } else {
                    mqtt.unsubscribe( theseTopics );
                }	  
    		  
    		} catch ( Exception ex ) {
    			System.out.println( "MQTT subscription exception caught !" );
    			System.out.println(ex.getMessage() + "\n" + 
    					"MQTT Subscription Exception");
    		}	
    	} else {
    		System.out.println( "MQTT client not connected !" );
    	}		
    }	
    

}
